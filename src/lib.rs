#![feature(try_trait)]

mod utils;

use crate::utils::set_panic_hook;
use std::cell::RefCell;
use std::collections::HashSet;
use std::rc::Rc;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

macro_rules! square {
    ($x: expr) => {
        $x * $x
    };
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct Color {
    r: u8,
    g: u8,
    b: u8,
    a: u8,
}

impl Color {
    fn new(r: u8, g: u8, b: u8, a: u8) -> Self {
        Color { r, g, b, a }
    }

    fn get_offset_color(&self, other: &Self, color_to_offset_from: &Self) -> Color {
        let dr = other.r.checked_sub(self.r).unwrap_or(0);
        let dg = other.g.checked_sub(self.g).unwrap_or(0);
        let db = other.b.checked_sub(self.b).unwrap_or(0);

        Color::new(
            color_to_offset_from.r.checked_add(dr).unwrap_or(255),
            color_to_offset_from.g.checked_add(dg).unwrap_or(255),
            color_to_offset_from.b.checked_add(db).unwrap_or(255),
            color_to_offset_from.a,
        )
    }

    fn get_squared_distance(&self, other: &Self) -> usize {
        let sr = self.r as isize;
        let sg = self.g as isize;
        let sb = self.b as isize;

        let or = other.r as isize;
        let og = other.g as isize;
        let ob = other.b as isize;

        (square!((sr - or)) + square!((sg - og)) + square!((sb - ob))) as usize
    }
}


#[derive(Debug, Clone)]
struct Point {
    x: usize,
    y: usize,
    color: Option<Color>,
    image_data: Rc<RefCell<Vec<u8>>>,
    image_data_len: usize,
    image_width: usize,
    image_height: usize,
    index: usize,
}

impl Point {
    pub fn new(
        x: usize,
        y: usize,
        image_data: Rc<RefCell<Vec<u8>>>,
        image_width: usize,
        image_height: usize,
        image_data_len: usize,
    ) -> Self {
        let index = ((y * image_width) + x) * 4;
        let mut p = Point {
            x,
            y,
            color: None,
            image_data,
            image_data_len,
            image_width,
            image_height,
            index,
        };
        p.init_color();
        p
    }


    fn init_color(&mut self) {
        let image_data = self.image_data.borrow();
        self.color = Some(Color {
            // r: *image_data.get(self.index).unwrap_or(&0),
            // g: *image_data.get(self.index+1).unwrap_or(&0),
            // b: *image_data.get(self.index+2).unwrap_or(&0)
            r: image_data[self.index],
            g: image_data[self.index + 1],
            b: image_data[self.index + 2],
            a: image_data[self.index + 3],
        });
    }

    fn get_offset_point(&self, dx: isize, dy: isize) -> Option<Self> {
        let new_x = self.x as isize + dx;
        let new_y = self.y as isize + dy;

        if new_x < 0
            || new_x >= self.image_width as isize
            || new_y < 0
            || new_y >= self.image_height as isize
        {
            return None;
        }
        let index = ((new_x * new_y) + new_x) * 4;

        if index >= (self.image_data_len) as isize {
            return None;
        }

        Some(Point::new(
            new_x as usize,
            new_y as usize,
            self.image_data.clone(),
            self.image_width,
            self.image_height,
            self.image_data_len,
        ))
    }

    fn get_neighbors(&self) -> Vec<Self> {
        let neighbors = vec![
            self.get_offset_point(-1, -1),
            self.get_offset_point(0, -1),
            self.get_offset_point(1, -1),
            self.get_offset_point(-1, 0),
            self.get_offset_point(1, 0),
            self.get_offset_point(1, -1),
            self.get_offset_point(1, 0),
            self.get_offset_point(1, 1),
        ];

        let mut real_neighbors = vec![];
        for neighbor in neighbors {
            if neighbor.is_some() {
                real_neighbors.push(neighbor.unwrap());
            }
        }
        real_neighbors
    }
}


#[wasm_bindgen]
pub fn flood_fill_get_region(
    point_x: usize,
    point_y: usize,
    tolerence: usize,
    image_data: Vec<u8>,
    length: usize,
    image_width: usize,
    image_height: usize,
) -> Vec<usize> {
    set_panic_hook();
    if point_x > image_width || point_y > image_height {
        return vec![];
    }

    // let color = Color::new(r, g, b, a);

    let image_data = Rc::from(RefCell::new(image_data));
    let init_point = Point::new(
        point_x,
        point_y,
        image_data,
        image_width,
        image_height,
        length,
    );
    let init_color = init_point.color.unwrap();
    let mut neighbors = vec![init_point.clone()];
    let mut points_used = vec![];
    let mut used_points = HashSet::new();
    used_points.insert(init_point.index);

    while !neighbors.is_empty() {
        let mut new_neighbors = Vec::new();
        for neighbor in neighbors {
            // used_points.insert(neighbor.index);
            let neighborneightbors = neighbor.get_neighbors();
            let mut valid_neightborneightbors = vec![];
            for neighborneightbor in neighborneightbors {
                if neighborneightbor
                    .color
                    .unwrap()
                    .get_squared_distance(&init_color)
                    < tolerence
                {
                    valid_neightborneightbors.push(neighborneightbor.clone());
                    if !used_points.contains(&neighborneightbor.index) {
                        new_neighbors.push(neighborneightbor.clone());
                    }
                }
                used_points.insert(neighborneightbor.index);
            }
        }
        neighbors = new_neighbors;
    }

    for point in used_points {
        points_used.push(point);
    }

    points_used
}


#[wasm_bindgen]
pub fn flood_fill_get_border(
    point_x: usize,
    point_y: usize,
    tolerence: usize,
    image_data: Vec<u8>,
    length: usize,
    image_width: usize,
    image_height: usize,
) -> Vec<usize> {
    set_panic_hook();
    if point_x > image_width || point_y > image_height {
        return vec![];
    }

    // let color = Color::new(r, g, b, a);

    let image_data = Rc::from(RefCell::new(image_data));
    let init_point = Point::new(
        point_x,
        point_y,
        image_data,
        image_width,
        image_height,
        length,
    );
    let init_color = init_point.color.unwrap();
    let mut neighbors = vec![init_point.clone()];
    let mut points_on_edge = vec![];
    let mut used_points = HashSet::new();
    let mut edge_points = HashSet::new();
    used_points.insert(init_point.index);

    while !neighbors.is_empty() {
        let mut new_neighbors = Vec::new();
        for neighbor in neighbors {
            // used_points.insert(neighbor.index);
            let neighborneightbors = neighbor.get_neighbors();
            let mut valid_neightborneightbors = vec![];
            for neighborneightbor in neighborneightbors {
                if neighborneightbor
                    .color
                    .unwrap()
                    .get_squared_distance(&init_color)
                    < tolerence
                {
                    valid_neightborneightbors.push(neighborneightbor.clone());
                    if !used_points.contains(&neighborneightbor.index) {
                        new_neighbors.push(neighborneightbor.clone());
                    }
                }
                used_points.insert(neighborneightbor.index);
            }
            if valid_neightborneightbors.len() < 8 {
                edge_points.insert(neighbor.index);
            }
        }
        neighbors = new_neighbors;
    }

    for point in edge_points {
        points_on_edge.push(point);
    }

    points_on_edge
}